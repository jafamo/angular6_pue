﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Bases;
using System;
using System.Collections.Generic;
using System.Text;

namespace Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas
{
    public class Alumno : EntityBase
    {
        public String Nombre{get;set;}
        public String Email { get; set; }
        
    }
}
