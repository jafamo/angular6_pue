﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Bases;
using System;
using System.Collections.Generic;
using System.Text;

namespace Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas
{
   public  class Pregunta:EntityBase
    {
        public String Texto { get; set; }
        public String RespuestaLibre { get; set; }
        public bool? EsEvaluable { get; set; }
        public int? RespuestaEvaluable { get; set; }
        public virtual Categoria Categoria { get; set; }
        public Guid? CategoriaId { get; set; }

    }
}
