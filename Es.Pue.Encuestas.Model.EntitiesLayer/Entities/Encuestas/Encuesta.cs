﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Bases;
using System;
using System.Collections.Generic;
using System.Text;

namespace Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas
{
   public class Encuesta:EntityBase
    {
        public virtual Formador Formador { get; set; }
        public Guid? FormadorId { get; set; }
        public virtual Curso Curso { get; set; }
        public Guid? CursoId { get; set; }
        public virtual List<Pregunta> Preguntas { get; set; }
        public virtual List<Evaluacion> Evaluaciones { get; set; }
        public Encuesta()
        {
            this.Preguntas = new List<Pregunta>();
            this.Evaluaciones = new List<Evaluacion>();
        }

    }
}
