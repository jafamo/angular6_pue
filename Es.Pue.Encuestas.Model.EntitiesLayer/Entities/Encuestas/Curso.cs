﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Bases;
using System;
using System.Collections.Generic;
using System.Text;

namespace Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas
{
   public class Curso:EntityBase
    {
        public String Nombre { get; set; }
        public String Codigo { get; set; }
        public DateTime? Inicio { get; set; }
        public DateTime? Fin { get; set; }
        public virtual List<Alumno> Alumnos { get; set; }

        public Curso() {
            this.Alumnos = new List<Alumno>();
        }
    }
}
