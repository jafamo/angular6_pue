﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Bases;
using System;
using System.Collections.Generic;
using System.Text;

namespace Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas
{
    public class Evaluacion:EntityBase
    {
        public virtual Alumno Alumno { get; set; }
        public virtual Guid? AlumnoId { get; set; }
        public virtual List<Pregunta> Preguntas { get; set; }
        public DateTime? Fecha { get; set; }
        Evaluacion()
        {
            this.Preguntas = new List<Pregunta>();
        }
    }
}
