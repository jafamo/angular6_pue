﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Bases;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Authentications
{
 
  public class Login:EntityBase
    {
        public String LoginName { get; set; }
        public String Password { get; set; }
    }
}
