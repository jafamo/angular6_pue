﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Bases
{
    public abstract class EntityBase
    {
        public Guid Id { get; set; }
        public DateTime? DbInsertedDate{ get; set; }
        public DateTime? DeletedDate { get; set; }


        public EntityBase() {
            Id = Guid.NewGuid();
            DbInsertedDate = null;
            DeletedDate = null;
        }
    }
}
