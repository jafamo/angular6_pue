import { Injectable } from '@angular/core';
import { Curso } from './_encuestas/model/entitieslayer/entities/encuestas/curso';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { IServiceManager } from './_encuestas/model/servicelayer/manager/iServiceManager';
import { ServiceManager } from './_encuestas/model/servicelayer/manager/serviceManager';
import { PersistenceTechnologies } from './_encuestas/model/utilitieslayer/persistenceTechnologies';

@Injectable({
  providedIn: 'root'
})
export class AppDataService {

  private cursos: Curso[]=null;
  private serviceManager:IServiceManager

  constructor(private http: HttpClient) { 
    this.serviceManager=new ServiceManager();
  }
  
  getServiceManager():IServiceManager{
    return this.serviceManager;
  }

  getCursos(fromCache: Boolean = true): Observable<Curso[]> {

    if (this.cursos == null || !fromCache) {
     
      this.cursos=null;     
      return this.serviceManager
      .getCursoService()
      .getCursos(PersistenceTechnologies.REST,this.http)
      .pipe(
        tap(
          (data: Curso[]) => {
            this.cursos = data;
          }
        ));
    }
    else {
      return of(this.cursos)
    }
  }

 getCursoById(id: String, fromCache: Boolean = true): Observable<Curso> {
    let curso = null
    this.getCursos(fromCache).subscribe(
      (data: Curso[]) => {
        curso = data.find(curso => curso.id === id);
      },
      error => {
        console.log(JSON.stringify(error));
      }
    );
    return of(curso)
  }

  saveCurso(curso:Curso): Observable<Curso>{
   
    return this.serviceManager
    .getCursoService()
    .saveCurso(curso,PersistenceTechnologies.REST,this.http);

  }

}

