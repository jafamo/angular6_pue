import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { MainLayoutComponent } from "./shared/layout/app-layouts/main-layout.component";
import { AuthLayoutComponent } from "./shared/layout/app-layouts/auth-layout.component";
import { AuthGuard } from "./_encuestas/guards/authGuard";

const routes: Routes = [
  {
    path: "",
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    children: 
    [
      {
        path: "",
        redirectTo: "home",
        pathMatch: "full"
      },
      {
        path: "home",
        loadChildren: "./features/home/home.module#HomeModule"
      },
      {
        path: "cursos",
        loadChildren: "./_encuestas/components/cursos/cursos.module#CursosModule"
      },
      {
        path: "encuestas",
        loadChildren: "./_encuestas/components/encuestas/encuestas.module#EncuestasModule"
      }
    ]
  },
  {
    path: "",
    component: MainLayoutComponent,
    children: 
    [     
      {
        path: "auth",
        loadChildren: "./_encuestas/components/auth/auth.module#AuthModule"
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
