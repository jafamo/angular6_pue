import { Observable } from "rxjs";
import { PersistenceTechnologies } from "../../utilitieslayer/persistenceTechnologies";
import { Encuesta } from "../../entitieslayer/entities/encuestas/encuesta";

export interface IEncuestaService{

    saveEncuesta(encuesta:Encuesta,persistenceTechnology:PersistenceTechnologies,persistenceDependency:any ):Observable<Encuesta>;
    getEncuestas(persistenceTechnology:PersistenceTechnologies,persistenceDependency:any ):Observable<Encuesta[]>;
    getEncuestaById(id:String,persistenceTechnology:PersistenceTechnologies,persistenceDependency:any):Observable<Encuesta>;

}