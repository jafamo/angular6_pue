import { LoginDTO } from "@app/_encuestas/components/auth/loginDTO";
import { PersistenceTechnologies } from "../../utilitieslayer/persistenceTechnologies";
import { TokenDTO } from "@app/_encuestas/components/auth/tokenDTO";
import { Observable } from "rxjs";

export interface IAuthService{

    login(loginDto:LoginDTO,
        persistenceTechnology:PersistenceTechnologies
        ,persistenceDependency:any )
        :Observable<TokenDTO>;
   
}