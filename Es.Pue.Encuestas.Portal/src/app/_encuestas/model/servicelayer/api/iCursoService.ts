import { Observable } from "rxjs";
import { Curso } from "../../entitieslayer/entities/encuestas/curso";
import { PersistenceTechnologies } from "../../utilitieslayer/persistenceTechnologies";

export interface ICursoService{

    saveCurso(curso:Curso,persistenceTechnology:PersistenceTechnologies,persistenceDependency:any ):Observable<Curso>;
    getCursos(persistenceTechnology:PersistenceTechnologies,persistenceDependency:any ):Observable<Curso[]>;
    getCursoById(id:String,persistenceTechnology:PersistenceTechnologies,persistenceDependency:any):Observable<Curso>;

}