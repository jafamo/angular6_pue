import { ICursoService } from "../api/iCursoService";
import { Curso } from "../../entitieslayer/entities/encuestas/curso";
import { PersistenceTechnologies } from "../../utilitieslayer/persistenceTechnologies";
import { Observable } from "rxjs";
import { ServiceBase } from "./serviceBase";
import { Encuesta } from "../../entitieslayer/entities/encuestas/encuesta";
import { IEncuestaService } from "../api/IEncuestaService";

export class EncuestaService extends ServiceBase implements IEncuestaService{
   
 
    saveEncuesta(encuesta:Encuesta,
        persistenceTechnology:PersistenceTechnologies,
        persistenceDependency:any)
         :Observable<Encuesta> {
           
           return this.getPersistenceManager(persistenceTechnology)
            .getEncuestaDAO()
            .saveEncuesta(encuesta,persistenceDependency);
    }    
    
    getEncuestas(persistenceTechnology:PersistenceTechnologies,
        persistenceDependency:any)
    :Observable<Encuesta[]> {
     
        return this.getPersistenceManager(persistenceTechnology)
        .getEncuestaDAO()
        .getEncuestas(persistenceDependency);

    }

    getEncuestaById(id: String,persistenceTechnology:PersistenceTechnologies,
        persistenceDependency:any)
    :Observable<Encuesta> {
        return this.getPersistenceManager(persistenceTechnology)
        .getEncuestaDAO()
        .getEncuestaById(id,persistenceDependency);
    }


}