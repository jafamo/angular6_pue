import { PersistenceManager } from "../../persistencelayer/manager/persistenceManager";
import { PersistenceTechnologies } from "../../utilitieslayer/persistenceTechnologies";
import { LocalStoragePersistenceManager } from "../../persistencelayer/impl/localstorage/manager/localStoragePersistenceManager";
import { RestPersistenceManager } from "../../persistencelayer/impl/rest/manager/restPersistenceManager";


export abstract class ServiceBase{

    private persistenceManagers:PersistenceManager[];
    
    constructor(persistenceManagers:PersistenceManager[]){
        this.persistenceManagers=persistenceManagers;
    }

    protected getPersistenceManager(
        persistenceTechnology:PersistenceTechnologies):
        PersistenceManager{
        let persistenceManager:PersistenceManager=null        
         
        switch(persistenceTechnology){
             
            case PersistenceTechnologies.LOCAL_STORAGE:
              persistenceManager=
                this.persistenceManagers.find(
                    pm=>pm instanceof LocalStoragePersistenceManager)
              break;

              case PersistenceTechnologies.REST:
              persistenceManager=
              this.persistenceManagers.find(
                pm=>pm instanceof RestPersistenceManager)
              break;
          }
          return persistenceManager;
        }
    
}