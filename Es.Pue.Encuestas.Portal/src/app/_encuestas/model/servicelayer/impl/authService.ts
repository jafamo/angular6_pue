import { PersistenceTechnologies } from "../../utilitieslayer/persistenceTechnologies";
import { Observable } from "rxjs";
import { ServiceBase } from "./serviceBase";
import { IAuthService } from "../api/IAuthService";
import { LoginDTO } from "@app/_encuestas/components/auth/loginDTO";
import { TokenDTO } from "@app/_encuestas/components/auth/tokenDTO";
import { tap } from "rxjs/operators";


export class AuthService extends ServiceBase implements IAuthService{
   
    login(loginDto:LoginDTO,
         persistenceTechnology: PersistenceTechnologies,
         persistenceDependency: any): Observable<TokenDTO> {
       
            return this.getPersistenceManager(persistenceTechnology)
        .getAuthDAO().login(loginDto,persistenceDependency).pipe(tap(
            (data:TokenDTO)=>{
                sessionStorage.setItem('access_token',data.accessToken as string);
            }
        ));
    }
}