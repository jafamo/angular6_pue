import { ICursoService } from "../api/iCursoService";
import { Curso } from "../../entitieslayer/entities/encuestas/curso";
import { PersistenceTechnologies } from "../../utilitieslayer/persistenceTechnologies";
import { Observable } from "rxjs";
import { ServiceBase } from "./serviceBase";

export class CursoService extends ServiceBase implements ICursoService{
 
    saveCurso(curso:Curso,
        persistenceTechnology:PersistenceTechnologies,
        persistenceDependency:any)
         :Observable<Curso> {
           
           return this.getPersistenceManager(persistenceTechnology)
            .getCursoDAO()
            .saveCurso(curso,persistenceDependency);
    }    
    
    getCursos(persistenceTechnology:PersistenceTechnologies,
        persistenceDependency:any)
    :Observable<Curso[]> {
     
        return this.getPersistenceManager(persistenceTechnology)
        .getCursoDAO()
        .getCursos(persistenceDependency);

    }

    getCursoById(id: String,persistenceTechnology:PersistenceTechnologies,
        persistenceDependency:any)
    :Observable<Curso> {
        return this.getPersistenceManager(persistenceTechnology)
        .getCursoDAO()
        .getCursoById(id,persistenceDependency);
    }


}