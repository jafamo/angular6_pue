import { IServiceManager } from "./iServiceManager";
import { ICursoService } from "../api/iCursoService";
import { CursoService } from "../impl/cursoService";
import { PersistenceManager } from "../../persistencelayer/manager/persistenceManager";
import { PersistenceTechnologies } from "../../utilitieslayer/persistenceTechnologies";
import { LocalStoragePersistenceManager } from "../../persistencelayer/impl/localstorage/manager/localStoragePersistenceManager";
import { RestPersistenceManager } from "../../persistencelayer/impl/rest/manager/restPersistenceManager";
import { IAuthService } from "../api/IAuthService";
import { AuthService } from "../impl/authService";
import { IEncuestaService } from "../api/IEncuestaService";
import { EncuestaService } from "../impl/encuestaService";


export class ServiceManager implements IServiceManager {

    constructor() {
        this.persistenceManagers = new Array<PersistenceManager>();
        this.persistenceManagers
            .push(new LocalStoragePersistenceManager());
        this.persistenceManagers
            .push(new RestPersistenceManager());

    }

    private cursoService: ICursoService = null;
    private authService: IAuthService = null;
    private encuestaService: IEncuestaService = null;


    
    private persistenceManagers: PersistenceManager[] = null;

    getCursoService(): ICursoService {

        if (this.cursoService == null) {
            this.cursoService = new CursoService
                (this.persistenceManagers);
        }
        return this.cursoService;
    }


    getAuthService(): IAuthService {

        if (this.authService == null) {
            this.authService = new AuthService
                (this.persistenceManagers);
        }
        return this.authService;
    }

    getEncuestaService(): IEncuestaService {

        if (this.encuestaService == null) {
            this.encuestaService = new EncuestaService
                (this.persistenceManagers);
        }
        return this.encuestaService;
    }

}