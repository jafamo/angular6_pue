import { ICursoService } from "../api/iCursoService";
import { IAuthService } from "../api/IAuthService";
import { IEncuestaService } from "../api/IEncuestaService";

export interface IServiceManager{
    
    getCursoService():ICursoService
    getAuthService():IAuthService
    getEncuestaService():IEncuestaService
}