import { Formador } from "./formador";
import { Curso } from "./curso";
import { Pregunta } from "./pregunta";
import { Evaluacion } from "./evaluacion";
import { EntityBase } from "../bases/entityBase";

export class Encuesta extends EntityBase{
    
    formador:Formador=null;
    formadorId:String=null;
    curso:Curso=null;
    cursoId:String=null;
    preguntas:Pregunta[];
    evaluaciones:Evaluacion[];


    constructor(){
        super();
        this.preguntas= new Array<Pregunta>();
        this.evaluaciones= new Array<Evaluacion>();
    }

}