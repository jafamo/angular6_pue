import { Alumno } from "./alumno";
import { EntityBase } from "../bases/entityBase";

export class Curso extends EntityBase{
    nombre:String=null;
    codigo:String=null;
    inicio:Date=null;
    fin:Date=null;
    alumnos:Alumno[];

    constructor(){
        super();
        this.alumnos=new Array<Alumno>();
    }
}