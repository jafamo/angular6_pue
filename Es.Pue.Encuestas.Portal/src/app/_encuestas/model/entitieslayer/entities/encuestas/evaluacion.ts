import { Alumno } from "./alumno";
import { Pregunta } from "./pregunta";
import { EntityBase } from "../bases/entityBase";

export class Evaluacion extends EntityBase{
    
    alumno:Alumno=null;
    preguntas:Pregunta[];
    fecha:Date=null;

    constructor(){
        super();
        this.preguntas=new Array<Pregunta>();
    }
}