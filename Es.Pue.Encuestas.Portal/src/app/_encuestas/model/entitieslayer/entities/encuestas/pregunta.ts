import { Categoria } from "./categoria";
import { EntityBase } from "../bases/entityBase";

export class Pregunta extends EntityBase{
    
    texto:String=null;
    esEvaluable: Boolean=null;
    respuestaEvaluable:Number=null;
    respuestaLibre:String=null;
    categoria:Categoria=null;

}