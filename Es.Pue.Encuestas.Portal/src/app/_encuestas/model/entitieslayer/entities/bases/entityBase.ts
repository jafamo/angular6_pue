import { AppUtilities } from "@app/_encuestas/model/utilitieslayer/appUtilities";

export class EntityBase{
    id:String=null;
    dBInsertedDate:Date=null;
    deletedDate:Date=null;

    constructor(){
        this.id=AppUtilities.newGuid();
    }
    
}