import { Observable } from "rxjs";
import { Encuesta } from "../../entitieslayer/entities/encuestas/encuesta";

export interface IEncuestaDAO{
    saveEncuesta(encuesta:Encuesta,persistenceDependency:any):Observable<Encuesta>;
    getEncuestas(persistenceDependency:any):Observable<Encuesta[]>;
    getEncuestaById(id:String,persistenceDependency:any):Observable<Encuesta>;
}