import { Curso } from "../../entitieslayer/entities/encuestas/curso";
import { Observable } from "rxjs";

export interface ICursoDAO{
    saveCurso(curso:Curso,persistenceDependency:any):Observable<Curso>;
    getCursos(persistenceDependency:any):Observable<Curso[]>;
    getCursoById(id:String,persistenceDependency:any):Observable<Curso>;
}