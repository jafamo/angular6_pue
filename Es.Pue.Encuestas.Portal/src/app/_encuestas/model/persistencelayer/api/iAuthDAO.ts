import { Curso } from "../../entitieslayer/entities/encuestas/curso";
import { Observable } from "rxjs";
import { LoginDTO } from "@app/_encuestas/components/auth/loginDTO";
import { TokenDTO } from "@app/_encuestas/components/auth/tokenDTO";

export interface IAuthDAO{
    login(loginDto:LoginDTO,persistenceDependency:any):Observable<TokenDTO>;
}