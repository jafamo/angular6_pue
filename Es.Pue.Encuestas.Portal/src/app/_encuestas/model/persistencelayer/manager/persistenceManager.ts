import { ICursoDAO } from "../api/iCursoDAO";
import { IAuthDAO } from "../api/iAuthDAO";
import { IEncuestaDAO } from "../api/iEncuestaDAO";

export abstract class PersistenceManager{

    protected cursoDAO:ICursoDAO;
    abstract getCursoDAO():ICursoDAO 

    protected authDAO:IAuthDAO;
    abstract getAuthDAO():IAuthDAO 

    protected encuestaDAO:IEncuestaDAO;
    abstract getEncuestaDAO():IEncuestaDAO 

}