import { Observable, of } from "rxjs";
import { Encuesta } from "@app/_encuestas/model/entitieslayer/entities/encuestas/encuesta";
import { IEncuestaDAO } from "../../../api/iEncuestaDAO";


export class EncuestaDAO implements IEncuestaDAO{
 
 
    saveEncuesta(encuesta:Encuesta,persistenceDependency:any): Observable<Encuesta> {

        let encuestasJson=persistenceDependency.getItem("encuestas");       
        let encuestas:Encuesta[]=JSON.parse(encuestasJson);
        if(encuestas==null){
            encuestas=new Array<Encuesta>();
        }
        let indiceEncuesta=encuestas.findIndex(c=>c.id==encuesta.id)

        if(indiceEncuesta==-1){
            encuestas.push(encuesta)
        }else{
            encuestas[indiceEncuesta]=encuesta    
        }
        persistenceDependency.setItem("encuestas",JSON.stringify(encuestas));
        
        return of(encuesta);
    }  
    
    getEncuestas(persistenceDependency:any):Observable<Encuesta[]> {
        let encuestasJson=persistenceDependency.getItem("encuesta");
        let encuestas=JSON.parse(encuestasJson);
        return of(encuestas)
    }

   
    getEncuestaById(id: String,persistenceDependency:any): Observable<Encuesta> {
        let encuestasJson=persistenceDependency.getItem("encuestas");
        let encuestas:Encuesta[]=JSON.parse(encuestasJson);
        if(encuestas==null){
            encuestas=new Array<Encuesta>();
        }
        let encuesta= encuestas.find(c=>c.id==id);    
        return of(encuesta);
    }

}