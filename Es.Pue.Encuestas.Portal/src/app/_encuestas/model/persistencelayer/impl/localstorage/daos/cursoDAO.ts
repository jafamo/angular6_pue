import { ICursoDAO } from "../../../api/iCursoDAO";
import { Curso } from "@app/_encuestas/model/entitieslayer/entities/encuestas/curso";
import { Observable, of } from "rxjs";


export class CursoDAO implements ICursoDAO{
 
 
    saveCurso(curso:Curso,persistenceDependency:any): Observable<Curso> {

        let cursosJson=persistenceDependency.getItem("cursos");       
        let cursos:Curso[]=JSON.parse(cursosJson);
        if(cursos==null){
            cursos=new Array<Curso>();
        }
        let indiceCurso=cursos.findIndex(c=>c.id==curso.id)

        if(indiceCurso==-1){
            cursos.push(curso)
        }else{
            cursos[indiceCurso]=curso    
        }
        persistenceDependency.setItem("cursos",JSON.stringify(cursos));
        
        return of(curso);
    }  
    
    getCursos(persistenceDependency:any):Observable<Curso[]> {
        let cursosJson=persistenceDependency.getItem("cursos");
        let cursos=JSON.parse(cursosJson);
        return of(cursos)
    }

   
    getCursoById(id: String,persistenceDependency:any): Observable<Curso> {
        let cursosJson=persistenceDependency.getItem("cursos");
        let cursos:Curso[]=JSON.parse(cursosJson);
        if(cursos==null){
            cursos=new Array<Curso>();
        }
        let curso= cursos.find(c=>c.id==id);    
        return of(curso);
    }

}