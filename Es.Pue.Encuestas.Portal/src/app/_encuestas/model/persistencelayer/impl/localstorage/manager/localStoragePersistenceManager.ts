import { PersistenceManager } from "../../../manager/persistenceManager";
import { ICursoDAO } from "../../../api/iCursoDAO";
import { CursoDAO } from "../daos/cursoDAO";
import { IAuthDAO } from "../../../api/iAuthDAO";
import { IEncuestaDAO } from "../../../api/iEncuestaDAO";
import { EncuestaDAO } from "../daos/encuestaDAO";


export class LocalStoragePersistenceManager extends PersistenceManager{
    
    protected cursoDAO:ICursoDAO=null;
    protected encuestaDAO:IEncuestaDAO=null;
    
    getCursoDAO(): ICursoDAO {
     
        if(this.cursoDAO==null){
            this.cursoDAO=new CursoDAO();
        }
        
        return this.cursoDAO;
    }

    getEncuestaDAO(): IEncuestaDAO {
     
        if(this.encuestaDAO==null){
            this.encuestaDAO=new EncuestaDAO();
        }
        
        return this.encuestaDAO;
    }

    getAuthDAO(): IAuthDAO {
       
        return null;
     
    }
}