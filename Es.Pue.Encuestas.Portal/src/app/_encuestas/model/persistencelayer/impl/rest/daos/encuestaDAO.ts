import { IEncuestaDAO } from "../../../api/iEncuestaDAO";
import { Encuesta } from "@app/_encuestas/model/entitieslayer/entities/encuestas/encuesta";
import { Observable, of } from "rxjs";
import { HttpHeaders, HttpClient } from "@angular/common/http";

export class EncuestaDAO implements IEncuestaDAO{
   
    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
      }
    
 
    saveEncuesta(encuesta:Encuesta,persistenceDependency:any): Observable<Encuesta> {
        let http:HttpClient=persistenceDependency;
        let url='https://encuestasangular.azurewebsites.net/api/Encuestas/Encuesta'
        return http.post<Encuesta>(url,encuesta,this.httpOptions)
    }  
    
    getEncuestas(persistenceDependency:any):Observable<Encuesta[]> {
       
        let http:HttpClient=persistenceDependency;
        let url = 'https://encuestasangular.azurewebsites.net/api/Encuestas';
        return http.get<Encuesta[]>(url);
          
    }
   
    getEncuestaById(id: String,persistenceDependency:any): Observable<Encuesta> {
        let http:HttpClient=persistenceDependency;
        let url = 'https://encuestasangular.azurewebsites.net/api/Encuestas/Encuesta/'+id;
        return http.get<Encuesta>(url);
    }

}