import { ICursoDAO } from "../../../api/iCursoDAO";
import { Curso } from "@app/_encuestas/model/entitieslayer/entities/encuestas/curso";
import { Observable, of } from "rxjs";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { IAuthDAO } from "../../../api/iAuthDAO";
import { TokenDTO } from "@app/_encuestas/components/auth/tokenDTO";
import { LoginDTO } from "@app/_encuestas/components/auth/loginDTO";

export class AuthDAO implements IAuthDAO{
  
    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
      }


    login(loginDto: LoginDTO, persistenceDependency: any): Observable<TokenDTO> {

        let http:HttpClient=persistenceDependency;
        let url='https://encuestasangular.azurewebsites.net/api/login'
        return http.post<TokenDTO>(url,loginDto,this.httpOptions)
    }
   
 

}