import { ICursoDAO } from "../../../api/iCursoDAO";
import { Curso } from "@app/_encuestas/model/entitieslayer/entities/encuestas/curso";
import { Observable, of } from "rxjs";
import { HttpHeaders, HttpClient } from "@angular/common/http";

export class CursoDAO implements ICursoDAO{
   
    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
      }
    
 
    saveCurso(curso:Curso,persistenceDependency:any): Observable<Curso> {
        let http:HttpClient=persistenceDependency;
        let url='https://encuestasangular.azurewebsites.net/api/Cursos/Curso'
        return http.post<Curso>(url,curso,this.httpOptions)
    }  
    
    getCursos(persistenceDependency:any):Observable<Curso[]> {
       
        let http:HttpClient=persistenceDependency;
        let url = 'https://encuestasangular.azurewebsites.net/api/Cursos';
        return http.get<Curso[]>(url);
          
    }
   
    getCursoById(id: String,persistenceDependency:any): Observable<Curso> {
        let http:HttpClient=persistenceDependency;
        let url = 'https://encuestasangular.azurewebsites.net/api/Cursos/Curso/'+id;
        return http.get<Curso>(url);
    }

}