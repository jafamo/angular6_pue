import { PersistenceManager } from "../../../manager/persistenceManager";
import { ICursoDAO } from "../../../api/iCursoDAO";
import { CursoDAO } from "../daos/cursoDAO";
import { IAuthDAO } from "../../../api/iAuthDAO";
import { AuthDAO } from "../daos/authDAO";
import { IEncuestaDAO } from "../../../api/iEncuestaDAO";
import { EncuestaDAO } from "../daos/encuestaDAO";


export class RestPersistenceManager extends PersistenceManager{
    
    getAuthDAO(): IAuthDAO {
     
        if(this.authDAO==null){
            this.authDAO=new AuthDAO();
        }
        
        return this.authDAO;
    }
    
    getCursoDAO(): ICursoDAO {
     
        if(this.cursoDAO==null){
            this.cursoDAO=new CursoDAO();
        }
        
        return this.cursoDAO;
    }

    getEncuestaDAO(): IEncuestaDAO {
     
        if(this.encuestaDAO==null){
            this.encuestaDAO=new EncuestaDAO();
        }
        
        return this.encuestaDAO;
    }

}