import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppDataService } from '@app/app-data.service';
import { Curso } from '@app/_encuestas/model/entitieslayer/entities/encuestas/curso';
import { forkJoin } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Encuesta } from '@app/_encuestas/model/entitieslayer/entities/encuestas/encuesta';
import { Pregunta } from '@app/_encuestas/model/entitieslayer/entities/encuestas/pregunta';
import { PersistenceTechnologies } from '@app/_encuestas/model/utilitieslayer/persistenceTechnologies';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-encuesta-detalle',
  templateUrl: './detalle-encuesta.component.html',
  styleUrls: ['./detalle-encuesta.component.css']
})
export class DetalleEncuestaComponent implements OnInit {

  cursos:Curso[]=null;
  profesores:Curso[]=null;
  encuesta:Encuesta=null;
  id:String=null;
  
  form = new FormGroup({
    curso: new FormControl(null, Validators.required),
  });
 
 
  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private appData:AppDataService,private http:HttpClient) { }
  
  ngOnInit(){
    this.id = this.route.snapshot.paramMap.get('id');
    this.iniciarDatosComponente(this.id);
   
  }

  private iniciarDatosComponente(id){
  
    forkJoin(
      this.appData.getCursos(),
      this.appData.getCursos()
    ).subscribe(
      data=>{
          this.cursos=data[0];
          this.profesores=data[1];

         
  
        if(id){
          this.appData.
          getServiceManager()
          .getEncuestaService()
          .getEncuestaById(this.id,
            PersistenceTechnologies.REST,
            this.http).subscribe(
              data=>{
                this.encuesta=data
                this.form.controls['curso']
                .setValue(this.encuesta.curso)
            }
            )
        }
      }
    );
    }
 
   compare(c1:any,c2:any):boolean {
     return (c1&&c2)?c1.id===c2.id:c1===c2;
   }

  onSubmit(){
    if(this.form.valid){
      if(this.encuesta==null){ this.encuesta= new Encuesta();}
    this.encuesta.curso=this.form.get('curso').value;
    this.encuesta.cursoId=this.encuesta.curso.id;
    this.encuesta.preguntas.push(new Pregunta());
    }
  } 

  asignarPregunta(pregunta:Pregunta){

   let indice= this.encuesta.preguntas.findIndex(p=>p.id==pregunta.id);

    if(indice>-1){
      this.encuesta.preguntas[indice]=pregunta;
    }

    this.appData.
    getServiceManager()
    .getEncuestaService()
    .saveEncuesta(this.encuesta,PersistenceTechnologies.REST,this.http).subscribe(
      data=>{
        this.encuesta=data
      }
    );

  }

}
