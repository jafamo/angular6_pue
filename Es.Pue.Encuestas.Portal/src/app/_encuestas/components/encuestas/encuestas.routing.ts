import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { DetalleEncuestaComponent } from "./detalle/detalle-encuesta.component";
import { ListadoEncuestasComponent } from "./listado/listado-encuestas.component";


export const encuestasRoutes: Routes = [
    {
        path: '',
        component: ListadoEncuestasComponent,
        data: {
            pageTitle: 'Encuestas'
        }
    },
    {
        path: 'encuesta/:id',
        component: DetalleEncuestaComponent,
        data: {
            pageTitle: 'Encuesta'
        }
    },
    {
        path: 'encuesta',
        component: DetalleEncuestaComponent,
        data: {
            pageTitle: 'Encuesta'
        }
    }
];

export const encuestasRouting: ModuleWithProviders = RouterModule.forChild(encuestasRoutes);

