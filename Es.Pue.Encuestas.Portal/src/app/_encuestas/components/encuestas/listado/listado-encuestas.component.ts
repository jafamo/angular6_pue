import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppDataService } from '@app/app-data.service';
import { HttpClient } from '@angular/common/http';
import { PersistenceTechnologies } from '@app/_encuestas/model/utilitieslayer/persistenceTechnologies';
import { Encuesta } from '@app/_encuestas/model/entitieslayer/entities/encuestas/encuesta';

@Component({
  selector: 'app-encuestas-listado',
  templateUrl: './listado-encuestas.component.html',
  styleUrls: ['./listado-encuestas.component.css']
})
export class ListadoEncuestasComponent implements OnInit {

  encuestas:Array<Encuesta>=null;

  constructor(private router:Router,
    private appData:AppDataService,
    private http:HttpClient) { }
  
  ngOnInit() {
    this.loadEncuestas();   
  }

  loadEncuestas(){
    this.appData
    .getServiceManager()
    .getEncuestaService()
    .getEncuestas(
      PersistenceTechnologies.REST,
      this.http).
      subscribe(
        data=>{
          this.encuestas=data;
        }
      );
  }

  encuestaSeleccionada(id){
    this.router.navigate(['encuestas','encuesta',id])
  }

}
