import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { Pregunta } from "@app/_encuestas/model/entitieslayer/entities/encuestas/pregunta";
import { FormGroup, FormControl, Validators } from "@angular/forms";



@Component({
  selector: 'app-pregunta',
  templateUrl: './pregunta.component.html',
  styleUrls: []
})
export class PreguntaComponent implements OnInit {

  @Input() pregunta: Pregunta;
  @Output() preguntaOut= new EventEmitter();

  formPregunta: FormGroup;

  ngOnInit(): void {

    this.formPregunta = new FormGroup({
      textoPregunta: new FormControl(this.pregunta.texto, Validators.required),
      preguntaEvaluable: new FormControl(!!this.pregunta.esEvaluable)
    });

  }
  onSubmit() {
    if (this.formPregunta.valid) {
      let pregunta = JSON.parse(JSON.stringify(this.pregunta));
      pregunta.texto = this.formPregunta.get('textoPregunta').value;
      pregunta.esEvaluable = this.formPregunta.get('preguntaEvaluable').value;
      this.preguntaOut.emit(pregunta);
    }

  }

}