import {Input, Component, Output, EventEmitter } from "@angular/core";
import { Encuesta } from "@app/_encuestas/model/entitieslayer/entities/encuestas/encuesta";

@Component({
    selector:'data-table',
    templateUrl:'./data-table.component.html'
})
export class DataTableComponent{

    @Input() encuestas:Array<Encuesta>;
    @Output() encuestaId= new EventEmitter();

    sortAsc:Boolean=true;
    currentSortProperty:String="id";

    deepValue(obj, path){
        for (var i=0, path=path.split('.'), len=path.length; i<len; i++){
            obj = obj[path[i]];
        };
        return obj;
    }

    sort(sortPropety:String){
        if(this.currentSortProperty==sortPropety){
            this.sortAsc=!this.sortAsc
        }else{
            this.sortAsc=true    
        }
       
        this.encuestas.sort(
            (a:Encuesta,b:Encuesta)=> {
            let prop1=this.deepValue(a,sortPropety);
            let prop2=this.deepValue(b,sortPropety);
 
            let orden=(prop1 > prop2)?1:(prop1 < prop2)?-1:0
            if(!this.sortAsc) orden*=-1
            return orden                 
        } 
        )
        this.currentSortProperty=sortPropety;

    }
    
    filterObject:any={
        id:'',
        curso:{
            codigo:'',
            nombre:''
        }
    }

    seleccionarEncuesta(id){
        this.encuestaId.emit(id)
    }

}