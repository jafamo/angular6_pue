import { PipeTransform, Pipe } from "@angular/core";


@Pipe({
    name:'dataTableFilterPipe',
    pure:false
})
export class DataTableFilterPipe implements PipeTransform{
 
    transform(items:Array<any>,filter:any):any {
             if(!items|| !filter){
                return items;
             }

             return items
             .filter(item=>item.id.indexOf(filter.id)>-1)
             .filter(item=>item.curso.codigo.toLowerCase().indexOf(filter.curso.codigo.toLowerCase())>-1)
             .filter(item=>item.curso.nombre.toLowerCase().indexOf(filter.curso.nombre.toLowerCase())>-1)
             

    }

}