import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@app/shared/shared.module";
import { SmartadminDatatableModule } from "@app/shared/ui/datatable/smartadmin-datatable.module";
import { ListadoEncuestasComponent } from "./listado/listado-encuestas.component";
import { DetalleEncuestaComponent } from "./detalle/detalle-encuesta.component";
import { encuestasRouting } from "./encuestas.routing";
import { ReactiveFormsModule } from "@angular/forms";
import { PreguntaComponent } from "./_components/pregunta/pregunta.component";
import { DataTableComponent } from "./_components/data-table/data-table.component";
import { DataTableFilterPipe } from "./_components/data-table/data-table-filter.pipe";



@NgModule({
    imports: [
      CommonModule,
      encuestasRouting,
        SharedModule,
        SmartadminDatatableModule,
        ReactiveFormsModule
    ],
    declarations: [
      ListadoEncuestasComponent,
      DetalleEncuestaComponent,
      PreguntaComponent,
      DataTableComponent,
      DataTableFilterPipe
    ]
  })
  export class EncuestasModule { }
  