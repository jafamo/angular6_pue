import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ListadoCursosComponent } from "./listado/listado-cursos.component";
import { DetalleCursoComponent } from "./detalle/detalle-curso.component";


export const cursosRoutes: Routes = [
    {
        path: '',
        component: ListadoCursosComponent,
        data: {
            pageTitle: 'Cursos'
        }
    },
    {
        path: 'curso/:id',
        component: DetalleCursoComponent,
        data: {
            pageTitle: 'Curso'
        }
    },
    {
        path: 'curso',
        component: DetalleCursoComponent,
        data: {
            pageTitle: 'Curso'
        }
    }
];

export const cursosRouting: ModuleWithProviders = RouterModule.forChild(cursosRoutes);

