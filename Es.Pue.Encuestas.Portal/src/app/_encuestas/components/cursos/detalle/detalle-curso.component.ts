import { Component, OnInit } from '@angular/core';
import { Curso } from '@app/_encuestas/model/entitieslayer/entities/encuestas/curso';
import { ActivatedRoute, Router, GuardsCheckEnd } from '@angular/router';
import { AppDataService } from '@app/app-data.service';
import { Location } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-curso-detalle',
  templateUrl: './detalle-curso.component.html',
  styleUrls: ['./detalle-curso.component.css']
})
export class DetalleCursoComponent implements OnInit {

  curso: Curso = null;
  id: String = null;
  guardando:boolean=false;

  constructor(private route: ActivatedRoute,
    private location: Location,
    private router: Router, private appData: AppDataService) { }

  ngOnInit() {
    this.cargarCursoSeleccionado();
  }

  cargarCursoSeleccionado() {

    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id == null) {
      this.curso = new Curso();
    } else {
      this.appData.getCursoById(this.id).subscribe(
        (data: Curso) => {
          this.curso =
            JSON.parse(JSON.stringify(data));
        },
        error => {
          console.log(JSON.stringify(error));
        }
      );
    }
  }


  public guardar() {

    this.guardando=true;

    this.appData.saveCurso(this.curso).subscribe(
      (data: Curso) => {
        this.appData.getCursos(false).subscribe(
          (data: Curso[]) => {
            this.router.navigate(['cursos']);           
          },
          error => {
            console.log(JSON.stringify(error));
            this.guardando=false;
          }
        )
      },
      error => {
        console.log(JSON.stringify(error));
        this.guardando=false;
      }
    );
  }

  public cancelar() {

    this.location.back();

  }

  public eliminar() {

    this.curso.deletedDate = new Date();
    this.guardar();
  }

}
