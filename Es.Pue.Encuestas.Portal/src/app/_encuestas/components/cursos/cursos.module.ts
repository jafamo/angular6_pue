import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@app/shared/shared.module";
import { cursosRouting } from "./cursos.routing";
import { SmartadminDatatableModule } from "@app/shared/ui/datatable/smartadmin-datatable.module";
import { ListadoCursosComponent } from "./listado/listado-cursos.component";
import { DetalleCursoComponent } from "./detalle/detalle-curso.component";



@NgModule({
    imports: [
      CommonModule,
      cursosRouting,
        SharedModule,
        SmartadminDatatableModule
    ],
    declarations: [ListadoCursosComponent,DetalleCursoComponent]
  })
  export class CursosModule { }
  