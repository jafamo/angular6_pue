import { Component, OnInit } from '@angular/core';
import { Curso } from '@app/_encuestas/model/entitieslayer/entities/encuestas/curso';
import { Alert } from 'selenium-webdriver';
import { Router } from '@angular/router';
import { AppDataService } from '@app/app-data.service';

@Component({
  selector: 'app-cursos-listado',
  templateUrl: './listado-cursos.component.html',
  styleUrls: ['./listado-cursos.component.css']
})
export class ListadoCursosComponent implements OnInit {

  options={}
  cursos:Curso[]; 

  constructor(private router:Router,private appData:AppDataService) { }

  ngOnInit() {
    this.iniciarDatosComponente();
    
  }

  private iniciarDatosComponente(){
   this.appData.getCursos().subscribe(
     (data:Curso[])=>{
      this.cursos=data;
      this.renderGrid();
     },
     error=>{
        console.log(JSON.stringify(error));
     }
   );
  }

  private renderGrid(){
      this.options={
        data:this.cursos.filter(c=>c.deletedDate==null),
        columns:[
          {data:"codigo"},
          {data:"nombre"},
          {data:"inicio"},
          {data:"fin"},
          {
            data:null,render:function(data,type,row){
              return `
              <a class="btn btn-primary" name="btnSeleccionarCurso" 
              data-element-id="${data.id}">sel</a>
              `
            }
          }
        ]
      }

      $(document).off('click','a[name=btnSeleccionarCurso]');
      $(document).on('click','a[name=btnSeleccionarCurso]',($event)=>{
          this.seleccionarCurso($($event.target).data("element-id"));
      });
  }

  private seleccionarCurso(id){

     this.router.navigate(['cursos/curso/'+id])
  }

}
