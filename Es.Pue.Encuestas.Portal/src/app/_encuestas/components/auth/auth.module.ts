import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@app/shared/shared.module";
import { authRouting } from "./auth.routing";
import { SmartadminDatatableModule } from "@app/shared/ui/datatable/smartadmin-datatable.module";
import { AuthComponent } from "./auth.component";


@NgModule({
    imports: [
      CommonModule,
      authRouting,
      SharedModule
    ],
    declarations: [AuthComponent]
  })
  export class AuthModule { }
  