import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router, GuardsCheckEnd } from '@angular/router';
import { AppDataService } from '@app/app-data.service';
import { Location } from '@angular/common';
import { LoginDTO } from './loginDTO';
import { PersistenceTechnologies } from '@app/_encuestas/model/utilitieslayer/persistenceTechnologies';
import { TokenDTO } from './tokenDTO';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: []
})
export class AuthComponent implements OnInit {

  loginDto:LoginDTO;
  peticionActiva:Boolean=false;

  constructor(private route: ActivatedRoute,
    private location: Location,
    private router: Router,
    private appData: AppDataService,
    private http:HttpClient) { }

  ngOnInit() {
    this.loginDto=new LoginDTO();
  }

  login(){
     
      this.appData.getServiceManager().getAuthService().login(
        this.loginDto,PersistenceTechnologies.REST,this.http
      ).subscribe(
        (data:TokenDTO)=>{
          this.router.navigate(['']);
        }
      );

  }

}
