import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { AuthComponent } from "./auth.component";


export const authRoutes: Routes = [
    {
        path: '',
        component: AuthComponent,
        data: {
            pageTitle: 'Login'
        }
    }
];

export const authRouting: 
ModuleWithProviders = 
RouterModule.forChild(authRoutes);

