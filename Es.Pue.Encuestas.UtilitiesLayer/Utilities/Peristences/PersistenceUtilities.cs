﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences
{
   public static class PersistenceUtilities
    {
        public enum PersistenceTechnologies { SqlAzure, TableStorageAzure, BlobsStorageAzure }

        public enum AzureBlobSASPermisions
        {
            Read,
            Read_List,
            Read_Write,
            Read_List_Write,
            Write,
            Write_List,
            List
        }
    }
}
