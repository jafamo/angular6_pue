﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Authentications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Es.Pue.Encuestas.Model.PersistenceLayer.Api.Authentications
{
    public interface ILoginDAO
    {
         Task<Login> GetLoginByLoginName(string loginName);
         void SaveLogin(Login login);
    }
}
