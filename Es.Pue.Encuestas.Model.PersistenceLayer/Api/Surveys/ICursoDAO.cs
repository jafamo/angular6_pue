﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Es.Pue.Encuestas.Model.PersistenceLayer.Api.Surveys
{
   public interface ICursoDAO
    {
        Task<Curso> GetCursoById(Guid id);
        Task<List<Curso>> GetCursos();
        void SaveCurso(Curso curso);

    }
}
