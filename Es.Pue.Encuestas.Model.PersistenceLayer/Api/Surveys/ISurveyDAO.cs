﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Es.Pue.Encuestas.Model.PersistenceLayer.Api.Encuestas
{
   public interface IEncuestaDAO
    {
        void SaveEncuesta(Encuesta survey);
        Task<Encuesta> GetEncuestaById(Guid id);
        Task<List<Encuesta>> GetEncuestas();
    }
}
