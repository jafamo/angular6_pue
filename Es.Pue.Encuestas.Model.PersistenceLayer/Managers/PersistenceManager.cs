﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Bases;
using Es.Pue.Encuestas.Model.PersistenceLayer.Api.Authentications;
using Es.Pue.Encuestas.Model.PersistenceLayer.Api.Encuestas;
using Es.Pue.Encuestas.Model.PersistenceLayer.Api.Surveys;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.BlobsAzure.Managers;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Managers;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.TableStorageAzure.Managers;
using System;
using System.Collections.Generic;
using System.Text;
using static Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences.PersistenceUtilities;

namespace Es.Pue.Encuestas.Model.PersistenceLayer.Managers
{
   public abstract class PersistenceManager:IDisposable
    {
        public static PersistenceManager GetPersistenceManager(PersistenceTechnologies persistenceTechnologies)
        {
            PersistenceManager persistenceManager = null;
            switch (persistenceTechnologies)
            {
                case PersistenceTechnologies.SqlAzure:
                    {
                        persistenceManager = new SqlAzurePersistenceManager();
                        break;
                    }
                case PersistenceTechnologies.TableStorageAzure:
                    {
                        persistenceManager = new TableStorageAzurePersistenceManager();
                        break;
                    }
                case PersistenceTechnologies.BlobsStorageAzure:
                    {
                        persistenceManager = new BlobAzurePersistenceManager();
                        break;
                    }
            }
            return persistenceManager;
        }

        public abstract void SaveChanges();

        public abstract void Dispose();

        public abstract T ReloadEntity<T>(T entityBase) where T : EntityBase;

        public abstract ILoginDAO GetLoginDAO();
        public abstract IEncuestaDAO GetEncuestaDAO();
        public abstract ICursoDAO GetCursoDAO();


    }
}
