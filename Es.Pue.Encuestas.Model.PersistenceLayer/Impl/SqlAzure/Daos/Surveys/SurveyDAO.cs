﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas;
using Es.Pue.Encuestas.Model.PersistenceLayer.Api.Encuestas;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Daos.Bases;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.DbContexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Daos.Encuestas
{
    public class EncuestaDAO : BaseDAO, IEncuestaDAO
    {
        public EncuestaDAO(EncuestasDbContext db) : base(db)
        {
        }

        public async Task<Encuesta> GetEncuestaById(Guid id)
        {
            return await this.db.Encuestas.AsNoTracking()
                         .Where(e => e.Id == id)
                         .FirstOrDefaultAsync();
               
        }

        public async Task<List<Encuesta>> GetEncuestas()
        {
            return await this.db.Encuestas.AsNoTracking()                  
                        .ToListAsync();
        }

        public void SaveEncuesta(Encuesta survey)
        {
            this.SetEntityState(survey);
        }
    }
}
