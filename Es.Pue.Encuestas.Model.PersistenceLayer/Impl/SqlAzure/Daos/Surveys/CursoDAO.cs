﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas;
using Es.Pue.Encuestas.Model.PersistenceLayer.Api.Surveys;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Daos.Bases;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.DbContexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Daos.Surveys
{
    class CursoDAO : BaseDAO,ICursoDAO
    {
        public CursoDAO(EncuestasDbContext db) : base(db)
        {
        }

        public  Task<Curso> GetCursoById(Guid id)
        {
            return  this.db.Cursos.FindAsync(id);

        }

        public Task<List<Curso>> GetCursos()
        {
            return this.db.Cursos.ToListAsync();
        }

        public void SaveCurso(Curso curso)
        {
            this.SetEntityState(curso);
        }
    }
}
