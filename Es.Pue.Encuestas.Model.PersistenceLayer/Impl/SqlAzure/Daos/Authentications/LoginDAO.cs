﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Authentications;
using Es.Pue.Encuestas.Model.PersistenceLayer.Api.Authentications;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Daos.Bases;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.DbContexts;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Daos.Authentications
{
    internal class LoginDAO : BaseDAO, ILoginDAO
    {
        public LoginDAO(EncuestasDbContext db) : base(db)
        {
        }

        public async Task<Login> GetLoginByLoginName(string loginName)
        {
            return await db.Logins.Where(l => l.LoginName == loginName).FirstOrDefaultAsync();
        }

        public void SaveLogin(Login login)
        {

            SetEntity(login);

        }
    }
}
