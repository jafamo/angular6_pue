﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Bases;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.DbContexts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Daos.Bases
{
    public abstract class BaseDAO
    {
        protected readonly EncuestasDbContext db;

        public BaseDAO(EncuestasDbContext db)
        {
            this.db = db;
        }

        public List<T> SetEntity<T>(List<T> list, bool remove = false)
        {
            List<T> result = null;
            if (list != null)
            {
                T[] listAux = new T[list.Count];
                list.CopyTo(listAux);

                for (int i = 0; i < listAux.Count(); i++)
                {
                    SetEntity(listAux[i], remove);
                }

                result = list.Where(entity => ((EntityBase)((object)entity)).DeletedDate == null).ToList();
            }

            return result;
        }


        public T SetEntity<T>(T entity, bool remove = false)
        {
            EntityBase entidadbase = (EntityBase)((object)entity);

            bool bAny = entidadbase.DbInsertedDate != null;
            EntityState state = EntityState.Unchanged;

            //Si la entidad se ha guardado ya y esta marcada como para eliminar, se borra de la bbdd

            if (entidadbase.DbInsertedDate != null && entidadbase.DeletedDate != null)
            {
                state = EntityState.Deleted;
            }
            //Si no se ha insertado aún y no esta marcado para eliminar, se inserta
            else if (entidadbase.DbInsertedDate == null && entidadbase.DeletedDate == null)
            {
                state = EntityState.Added;
            }
            //Si no se ha insertado en bbdd aún y pero esta marcado para eliminar, no haces nada.
            else if (entidadbase.DbInsertedDate == null && entidadbase.DeletedDate != null)
            {
                state = EntityState.Detached;
            }
            //Si ya esta en BBDD y no se ha de eliminar. lo modificamos
            else if (entidadbase.DbInsertedDate != null && entidadbase.DeletedDate == null)
            {
                state = EntityState.Modified;
            }

            db.Entry(entidadbase).State = state;

            return entity;
        }


        public EntityBase SetEntityState(EntityBase entity, bool updateForeignKeys = true)
        {
            if (!(entity is EntityBase))
            {
                return null;
            }

            if (updateForeignKeys)
            {
                UpdateForeignKeys(entity);
            }

            SetEntity(entity);

            foreach (System.Reflection.PropertyInfo prop in entity.GetType().GetProperties())
            {
                if (prop.GetValue(entity) != null)
                {
                    if (prop.GetValue(entity) is EntityBase)
                    {
                        SetEntityState((EntityBase)prop.GetValue(entity));
                    }
                    else if (prop.GetValue(entity).GetType().IsGenericType)
                    {
                        foreach (object element in (prop.GetValue(entity) as
                            IEnumerable<object>).Cast<object>().ToList())
                        {
                            if (element is EntityBase)
                            {
                                SetEntityState((EntityBase)element);
                            }
                        }
                    }
                }
            }

            return entity;
        }

        public EntityBase UpdateForeignKeys(EntityBase entity)
        {
            if (!(entity is EntityBase))
            {
                return null;
            }

            foreach (System.Reflection.PropertyInfo prop in entity.GetType().GetProperties())
            {
                if (prop.GetValue(entity) != null)
                {
                    if (prop.GetValue(entity) is EntityBase)
                    {
                        System.Reflection.PropertyInfo propId = entity.GetType().GetProperty($"{ prop.PropertyType.Name }Id");
                        propId.SetValue(entity,
                            ((EntityBase)
                            prop.GetValue(entity)).Id);
                        UpdateForeignKeys((EntityBase)prop.GetValue(entity));
                    }
                    else if (prop.GetValue(entity).GetType().IsGenericType)
                    {
                        foreach (object element in (prop.GetValue(entity) as
                            IEnumerable<object>).Cast<object>().ToList())
                        {
                            if (element is EntityBase)
                            {
                                UpdateForeignKeys((EntityBase)element);
                            }
                        }
                    }
                }
            }

            return entity;
        }


    }
}
