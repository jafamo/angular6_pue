﻿using Es.Pue.Encuestas.Model.PersistenceLayer.Api.Authentications;
using Es.Pue.Encuestas.Model.PersistenceLayer.Api.Encuestas;
using Es.Pue.Encuestas.Model.PersistenceLayer.Api.Surveys;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Daos.Authentications;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Daos.Encuestas;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Daos.Surveys;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.DbContexts;
using Es.Pue.Encuestas.Model.PersistenceLayer.Managers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Managers
{
    public class SqlAzurePersistenceManager : PersistenceManager
    {
        private readonly  EncuestasDbContext encuestasDbContext;
        private ILoginDAO loginDAO;
        private IEncuestaDAO surveyDAO;
        private ICursoDAO cursoDAO;

        public SqlAzurePersistenceManager()
        {
            encuestasDbContext = new EncuestasDbContext();
        }

        public override void Dispose()
        {
            this.encuestasDbContext.Dispose();
        }

        public override void SaveChanges()
        {
            this.encuestasDbContext.SaveChanges();
        }

        public override ILoginDAO GetLoginDAO()
        {
            return loginDAO ?? (loginDAO = new LoginDAO(encuestasDbContext));
        }

        public override IEncuestaDAO GetEncuestaDAO()
        {
            return surveyDAO ?? (surveyDAO = new EncuestaDAO(encuestasDbContext));
        }
        public override ICursoDAO GetCursoDAO()
        {
            return cursoDAO ?? (cursoDAO = new CursoDAO(encuestasDbContext));
        }
        public override T ReloadEntity<T>(T entityBase)
        {
            return this.encuestasDbContext.ReloadEntity(entityBase);
        }

    }
}
