﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Authentications;

namespace Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Mappers.Authentications
{
    public class LoginMAP : IEntityTypeConfiguration<Login>
    {
        public void Configure(EntityTypeBuilder<Login> builder)
        {
            builder.ToTable("Logins", "AUTHENTICATION");
            builder.Property(p => p.LoginName).HasMaxLength(50);
            builder.Property(p => p.Password).HasMaxLength(100);

        }
    }
}
