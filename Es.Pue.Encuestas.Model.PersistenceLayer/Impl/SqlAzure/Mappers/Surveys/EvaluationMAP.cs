﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Mappers.Encuestas
{
    public class EvaluationMAP : IEntityTypeConfiguration<Evaluation>
    {
        public void Configure(EntityTypeBuilder<Evaluation> builder)
        {
            builder.ToTable("Evaluations", "SURVEY");
        }
    }
}
