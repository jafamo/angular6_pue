﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Mappers.Encuestas
{
  public  class EncuestaMAP: IEntityTypeConfiguration<Encuesta>
    {
        public void Configure(EntityTypeBuilder<Encuesta> builder)
        {
            builder.ToTable("Encuestas", "SURVEY");
            builder.Property(p => p.EncuestaCode).HasMaxLength(200);
        }
    }
}

   

   