﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Authentications;
using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Bases;
using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Mappers.Authentications;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.DbContexts
{
    public class EncuestasDbContext : DbContext
    {
        public EncuestasDbContext() : base() { }
        public EncuestasDbContext(DbContextOptions<EncuestasDbContext> options)
            : base(options)
        { }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLazyLoadingProxies()
                .UseSqlServer("Server=tcp:intranetdb.database.windows.net,1433;Initial Catalog=Encuestas;Persist Security Info=False;User ID=intranet;Password=Pueangular00;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .ApplyConfiguration(new LoginMAP())
               
                ;

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var res = -1;
            try
            {
                var selectedEntityList = ChangeTracker.Entries()
                                                        .Where(x => x.Entity is EntityBase &&
                                                                    x.State == EntityState.Added).ToList();

                selectedEntityList.ForEach(entity => {
                    if (entity.State == EntityState.Added)
                    {
                        ((EntityBase)entity.Entity).DbInsertedDate = DateTime.Now;
                    }
                });
                res = base.SaveChanges();
            }
            catch (Exception exp)
            {
                throw exp;
            }


            return res;
        }
        
        public T ReloadEntity<T>(T entityBase) where T : EntityBase
        {

            try
            {
                var entities = this.ChangeTracker.Entries().ToList();
                entities?.ForEach(e => e.State = EntityState.Detached);
                var res = (T)this.Set<T>().Find(entityBase.Id);               
                return res;
            }
            catch (Exception exp)
            {

            }
            return null;
        }

        public DbSet<Login> Logins { get; set; }
        public DbSet<Encuesta> Encuestas { get; set; }
        public DbSet<Curso> Cursos { get; set; }
    }
}
