﻿using Es.Pue.Encuestas.Model.PersistenceLayer.Api.Authentications;
using Es.Pue.Encuestas.Model.PersistenceLayer.Api.Encuestas;
using Es.Pue.Encuestas.Model.PersistenceLayer.Api.Surveys;
using Es.Pue.Encuestas.Model.PersistenceLayer.Managers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Es.Pue.Encuestas.Model.PersistenceLayer.Impl.TableStorageAzure.Managers
{
    public class TableStorageAzurePersistenceManager : PersistenceManager
    {
        public override void Dispose()
        {
            
        }

        public override ILoginDAO GetLoginDAO()
        {
            throw new NotImplementedException();
        }

        public override IEncuestaDAO GetEncuestaDAO()
        {
            throw new NotImplementedException();
        }

        public override T ReloadEntity<T>(T entityBase)
        {
            throw new NotImplementedException();
        }

        public override void SaveChanges()
        {
            throw new NotImplementedException();
        }

        public override ICursoDAO GetCursoDAO()
        {
            throw new NotImplementedException();
        }
    }
}
