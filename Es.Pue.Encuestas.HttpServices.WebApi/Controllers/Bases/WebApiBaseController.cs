﻿using Es.Pue.Encuestas.Model.ServiceLayer.Managers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Es.Pue.Encuestas.HttpServices.WebApi.Controllers.Bases
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public abstract class WebApiBaseController : Controller
    {
        private ServiceManager serviceManager;
        
        public WebApiBaseController(){ }
        protected ServiceManager GetServiceManager()
        {
            return serviceManager ?? (serviceManager = new ServiceManager());
        }

        protected Guid? GetUserId()
        {
            var uid = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
            Guid userId;
            if (Guid.TryParse(uid, out userId))
            {
                return userId;
            }
            else
            {
                return null;
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                GetServiceManager()?.Dispose();
            }

            base.Dispose(disposing);
        }

    }
}
