﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Es.Pue.Encuestas.HttpServices.WebApi.Controllers.Bases;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Es.Pue.Encuestas.HttpServices.WebApi.Controllers
{

    [AllowAnonymous]

    [ResponseCache(Duration = 10, Location = ResponseCacheLocation.Any)]
    public class ValuesController : WebApiBaseController
    {

        private readonly IMemoryCache _cache;

        public ValuesController(IMemoryCache cache)
        {
            _cache = cache;
            
        }


     
        [HttpGet]
        public string Get()
        {
            return _cache.GetOrCreate<string>("IDGKey",
                cacheEntry => {
                  cacheEntry.SlidingExpiration = TimeSpan.FromSeconds(5);
                    return DateTime.Now.ToString();
                });
        }



        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
