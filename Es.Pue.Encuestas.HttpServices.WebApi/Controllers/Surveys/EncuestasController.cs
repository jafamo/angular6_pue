﻿using Es.Pue.Encuestas.HttpServices.WebApi.Controllers.Bases;
using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using static Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences.PersistenceUtilities;

namespace Es.Pue.Encuestas.HttpServices.WebApi.Controllers.Encuestas
{
    public class EncuestasController : WebApiBaseController
    {


        [HttpPost]
        [Route("Encuesta")]
        public ActionResult Save([FromBody] Encuesta survey)
        {

            GetServiceManager().GetEncuestaService().SaveEncuesta(survey, PersistenceTechnologies.SqlAzure);
            GetServiceManager().SaveChanges();
            survey = GetServiceManager().ReloadEntity(survey, PersistenceTechnologies.SqlAzure);

            return Ok(survey);
        }

        [HttpGet]
        [Route("Encuesta/{id}")]
        public async Task<ActionResult> GetEncuestaById(Guid id)
        {
            var survey = await GetServiceManager().GetEncuestaService().GetSurbeyById(id, PersistenceTechnologies.SqlAzure);

            return Ok(survey);
        }



        [HttpGet]
        [Route("")]
        public async Task<ActionResult> GetEncuestas()
        {
            var survey = await GetServiceManager().GetEncuestaService().GetEncuestas(PersistenceTechnologies.SqlAzure);

            return Ok(survey);
        }



    }
}
