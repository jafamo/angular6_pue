﻿using Es.Pue.Encuestas.HttpServices.WebApi.Controllers.Bases;
using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences.PersistenceUtilities;

namespace Es.Pue.Encuestas.HttpServices.WebApi.Controllers.Surveys
{

   
    public class CursosController : WebApiBaseController
    {

        [HttpPost]
        [Route("Curso")]
        public ActionResult Save([FromBody] Curso curso)
        {

            GetServiceManager().GetCursoService().SaveCurso(curso,
                PersistenceTechnologies.SqlAzure);

            GetServiceManager().SaveChanges();

            curso = GetServiceManager().ReloadEntity(curso, PersistenceTechnologies.SqlAzure);

            return Ok(curso);
        }

        [HttpGet]
        [Route("curso/{id}")]
        public async Task<ActionResult> GetCursoById(Guid id)
        {
            var curso = await GetServiceManager().GetCursoService().GetCursoById
                (id, PersistenceTechnologies.SqlAzure);

            return Ok(curso);
        }

        [HttpGet]
        [Route("")]
        public async Task<ActionResult> GetCursos()
        {
            var cursos = await GetServiceManager().GetCursoService().GetCursos
                (PersistenceTechnologies.SqlAzure);

            return Ok(cursos);
        }

    }
}
