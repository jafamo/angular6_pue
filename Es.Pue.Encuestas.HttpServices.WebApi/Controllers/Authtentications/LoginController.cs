﻿using Es.Pue.Encuestas.HttpServices.WebApi.Controllers.Bases;
using Es.Pue.Encuestas.HttpServices.WebApi.Models.Configurations;
using Es.Pue.Encuestas.HttpServices.WebApi.Models.ViewModels.Authentications;
using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Authentications;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences.PersistenceUtilities;

namespace Es.Pue.Encuestas.HttpServices.WebApi.Controllers.Authtentications
{
    [AllowAnonymous]
    public class LoginController: WebApiBaseController
    {
        private TokenJWTConfiguration tokenJWTConfiguration { get; set; }
        public LoginController(IOptions<TokenJWTConfiguration> tokenJWTConfiguration)
        {
            this.tokenJWTConfiguration = tokenJWTConfiguration.Value;
        }

        [HttpPost]
        [Route("")]
        public async Task<ActionResult<AccessTokenViewModel>> LogIn([FromBody] LoginViewModel loginViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);  
            }


            var login = await GetServiceManager()
                .GetLoginService().GetLoginByNameAndPassWord(loginViewModel.Login, loginViewModel.Password,PersistenceTechnologies.SqlAzure);

                if (login == null) return BadRequest("Usuario o password incorrectos");
          
                var accessToken = await CreateTokenAsync(login, DateTime.Now.AddDays(4000));

                return Ok(accessToken);
            

        }

        [HttpPost]
        [Route("save")]
        public ActionResult<Login> LogIn([FromBody] Login login)
        {
            GetServiceManager().GetLoginService().SaveLogin(login, PersistenceTechnologies.SqlAzure);
            GetServiceManager().SaveChanges();
            login.Password=null;
            return Ok(login);
        }


        private async Task<AccessTokenViewModel> CreateTokenAsync(Login login, DateTime expires)
        {
           
            string secretKey = tokenJWTConfiguration.SecretKey;
            string issuer = tokenJWTConfiguration.Iiuser;
            string audience = tokenJWTConfiguration.Audience;

            var claims = new List<Claim>
            {
                //Unique Id for token
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat,
                DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString(),ClaimValueTypes.Integer64),
                new Claim(JwtRegisteredClaimNames.Iss,issuer),
                new Claim(JwtRegisteredClaimNames.UniqueName,login.LoginName),
                new Claim(JwtRegisteredClaimNames.Sub,login.Id.ToString()),
                new Claim(ClaimTypes.Role,"AppUser")
            };

            /*if (usuario.Rol != null)
            {
                claims.Add(new Claim(ClaimTypes.Role, usuario.Rol.Nombre));
            }*/

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: issuer,
                audience: audience,
                claims: claims,
                notBefore: DateTime.Now,
                expires: expires,
                signingCredentials: creds
            );

            AccessTokenViewModel jwtToken = new AccessTokenViewModel();
            jwtToken.AccessToken = new JwtSecurityTokenHandler().WriteToken(token);    
            return jwtToken;

        }
    }
}
