﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Es.Pue.Encuestas.HttpServices.WebApi.Models.Configurations
{
    public class TokenJWTConfiguration
    {
        public String SecretKey { get; set; }
        public String Iiuser { get; set; }
        public String Audience { get; set; }
    }
}
