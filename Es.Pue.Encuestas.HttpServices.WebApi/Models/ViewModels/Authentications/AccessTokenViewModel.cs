﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Es.Pue.Encuestas.HttpServices.WebApi.Models.ViewModels.Authentications
{
    public class AccessTokenViewModel
    {
        public string AccessToken { get; set; }
    }
}
