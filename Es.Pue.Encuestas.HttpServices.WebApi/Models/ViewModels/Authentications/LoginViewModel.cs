﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Es.Pue.Encuestas.HttpServices.WebApi.Models.ViewModels.Authentications
{
    public class LoginViewModel
    {
        public String Login { get; set; }
        public String Password { get; set; }
    }
}
