﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Authentications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences.PersistenceUtilities;

namespace Es.Pue.Encuestas.Model.ServiceLayer.Api.Authentications
{
    public interface ILoginService
    {
        Task<Login> GetLoginByNameAndPassWord(string loginName, string password,PersistenceTechnologies persistenceTechnology);
        void SaveLogin(Login login,PersistenceTechnologies persistenceTechnology);
    }
}
