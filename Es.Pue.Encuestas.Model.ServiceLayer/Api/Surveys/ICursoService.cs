﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences.PersistenceUtilities;

namespace Es.Pue.Encuestas.Model.ServiceLayer.Api.Surveys
{
   public interface ICursoService
    {
        Task<Curso> GetCursoById(Guid id, PersistenceTechnologies persistenceTechnology);
        Task<List<Curso>> GetCursos(PersistenceTechnologies persistenceTechnology);
        void SaveCurso(Curso curso, PersistenceTechnologies persistenceTechnology);

    }
}
