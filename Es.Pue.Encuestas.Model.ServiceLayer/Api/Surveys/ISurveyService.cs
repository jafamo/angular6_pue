﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences.PersistenceUtilities;

namespace Es.Pue.Encuestas.Model.ServiceLayer.Api.Encuestas
{
    public interface IEncuestaService
    {
        void SaveEncuesta(Encuesta survey,PersistenceTechnologies persistenceTechnology);
        Task<Encuesta> GetSurbeyById(Guid id,PersistenceTechnologies persistenceTechnology);

        Task<List<Encuesta>> GetEncuestas( PersistenceTechnologies persistenceTechnology);
    }
}
