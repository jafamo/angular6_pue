﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Bases;
using Es.Pue.Encuestas.Model.ServiceLayer.Api.Authentications;
using Es.Pue.Encuestas.Model.ServiceLayer.Api.Encuestas;
using Es.Pue.Encuestas.Model.ServiceLayer.Api.Surveys;
using System;
using System.Collections.Generic;
using System.Text;
using static Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences.PersistenceUtilities;

namespace Es.Pue.Encuestas.Model.ServiceLayer.Managers
{
    public interface IServiceManager : IDisposable
    {
         ILoginService GetLoginService() ;
         IEncuestaService GetEncuestaService();
         ICursoService GetCursoService();
        void SaveChanges();
         T ReloadEntity<T>(T entityBase,PersistenceTechnologies persistenceTechnology) where T : EntityBase;


    }
}
