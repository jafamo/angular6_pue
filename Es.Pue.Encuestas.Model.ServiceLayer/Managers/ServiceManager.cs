﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Bases;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.BlobsAzure.Managers;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Managers;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.TableStorageAzure.Managers;
using Es.Pue.Encuestas.Model.PersistenceLayer.Managers;
using Es.Pue.Encuestas.Model.ServiceLayer.Api.Authentications;
using Es.Pue.Encuestas.Model.ServiceLayer.Api.Encuestas;
using Es.Pue.Encuestas.Model.ServiceLayer.Api.Surveys;
using Es.Pue.Encuestas.Model.ServiceLayer.Impl.Authentications;
using Es.Pue.Encuestas.Model.ServiceLayer.Impl.Encuestas;
using Es.Pue.Encuestas.Model.ServiceLayer.Impl.Surveys;
using static Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences.PersistenceUtilities;

namespace Es.Pue.Encuestas.Model.ServiceLayer.Managers
{
   public class ServiceManager : IServiceManager
    {
        List<PersistenceManager> persistenceManagers;

        public ServiceManager()
        {
            persistenceManagers = new List<PersistenceManager>();
            persistenceManagers.Add(PersistenceManager.GetPersistenceManager(PersistenceTechnologies.SqlAzure));
            persistenceManagers.Add(PersistenceManager.GetPersistenceManager(PersistenceTechnologies.TableStorageAzure));
            persistenceManagers.Add(PersistenceManager.GetPersistenceManager(PersistenceTechnologies.BlobsStorageAzure));
        }

        private ILoginService loginService;
        private IEncuestaService surveyService;
        private ICursoService cursoService;

        public ILoginService GetLoginService()
        {
            return loginService ?? (loginService = new LoginService(persistenceManagers));
        }
        public IEncuestaService GetEncuestaService()
        {
            return surveyService ?? (surveyService = new EncuestaService(persistenceManagers));
        }
        public ICursoService GetCursoService()
        {
            return cursoService ?? (cursoService = new CursoService(persistenceManagers));
        }
        public void Dispose()
        {
            persistenceManagers?.ForEach(pm => pm.Dispose());
        }
        public void SaveChanges()
        {
            persistenceManagers.Where(pm => pm is SqlAzurePersistenceManager).FirstOrDefault()?.SaveChanges();
        }

        public T ReloadEntity<T>(T entityBase,PersistenceTechnologies persistenceTechnology) where T : EntityBase
        {
           return GetPersistenceManager(persistenceTechnology).ReloadEntity(entityBase);
        }

        private PersistenceManager GetPersistenceManager(PersistenceTechnologies persistencesTechnologies)
        {
            PersistenceManager persistenceManager = null;
            switch (persistencesTechnologies)
            {
                case PersistenceTechnologies.SqlAzure:
                    persistenceManager = persistenceManagers.Where(pm => pm is SqlAzurePersistenceManager).FirstOrDefault();
                    break;
                case PersistenceTechnologies.BlobsStorageAzure:
                    persistenceManager = persistenceManagers.Where(pm => pm is BlobAzurePersistenceManager).FirstOrDefault();
                    break;
                case PersistenceTechnologies.TableStorageAzure:
                    persistenceManager = persistenceManagers.Where(pm => pm is TableStorageAzurePersistenceManager).FirstOrDefault();
                    break;
            }
            return persistenceManager;
        }

      
    }
}
