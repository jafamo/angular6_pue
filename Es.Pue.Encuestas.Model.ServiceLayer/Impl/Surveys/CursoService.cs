﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas;
using Es.Pue.Encuestas.Model.PersistenceLayer.Managers;
using Es.Pue.Encuestas.Model.ServiceLayer.Api.Surveys;
using Es.Pue.Encuestas.Model.ServiceLayer.Impl.Bases;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences.PersistenceUtilities;

namespace Es.Pue.Encuestas.Model.ServiceLayer.Impl.Surveys
{
    class CursoService : ServiceBase, ICursoService
    {
        public CursoService(List<PersistenceManager> persistenceManagers) : base(persistenceManagers)
        {
        }

        public Task<Curso> GetCursoById(Guid id, PersistenceTechnologies persistenceTechnology)
        {
            return  GetPersistenceManager(persistenceTechnology).GetCursoDAO().GetCursoById(id);

        }

        public Task<List<Curso>> GetCursos(PersistenceTechnologies persistenceTechnology)
        {
            return GetPersistenceManager(persistenceTechnology).GetCursoDAO().GetCursos();
        }

        public void SaveCurso(Curso curso, PersistenceTechnologies persistenceTechnology)
        {
             GetPersistenceManager(persistenceTechnology).GetCursoDAO().SaveCurso(curso);
        }
    }
}
