﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Encuestas;
using Es.Pue.Encuestas.Model.PersistenceLayer.Managers;
using Es.Pue.Encuestas.Model.ServiceLayer.Api.Encuestas;
using Es.Pue.Encuestas.Model.ServiceLayer.Impl.Bases;
using Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences.PersistenceUtilities;

namespace Es.Pue.Encuestas.Model.ServiceLayer.Impl.Encuestas
{
    class EncuestaService : ServiceBase, IEncuestaService
    {
        public EncuestaService(List<PersistenceManager> persistenceManagers) : base(persistenceManagers)
        {
        }

        public async Task<List<Encuesta>> GetEncuestas(PersistenceTechnologies persistenceTechnology)
        {
            return await GetPersistenceManager(persistenceTechnology).GetEncuestaDAO().GetEncuestas();

        }

        public async Task<Encuesta> GetSurbeyById(Guid id, PersistenceTechnologies persistenceTechnology)
        {
           return await GetPersistenceManager(persistenceTechnology).GetEncuestaDAO().GetEncuestaById(id);
        }

        public void SaveEncuesta(Encuesta survey, PersistenceTechnologies persistenceTechnology)
        {
            GetPersistenceManager(persistenceTechnology).GetEncuestaDAO().SaveEncuesta(survey);
        }


    }
}
