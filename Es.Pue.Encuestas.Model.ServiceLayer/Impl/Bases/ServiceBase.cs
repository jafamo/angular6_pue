﻿using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.BlobsAzure.Managers;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.SqlAzure.Managers;
using Es.Pue.Encuestas.Model.PersistenceLayer.Impl.TableStorageAzure.Managers;
using Es.Pue.Encuestas.Model.PersistenceLayer.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences.PersistenceUtilities;

namespace Es.Pue.Encuestas.Model.ServiceLayer.Impl.Bases
{
    abstract class ServiceBase
    {
        protected List<PersistenceManager> persistenceManagers;

        protected ServiceBase(List<PersistenceManager> persistenceManagers)
        {
            this.persistenceManagers = persistenceManagers;
        }

        protected PersistenceManager GetPersistenceManager(PersistenceTechnologies persistencesTechnologies)
        {
            PersistenceManager persistenceManager = null;
            switch (persistencesTechnologies)
            {
                case PersistenceTechnologies.SqlAzure:
                    persistenceManager = persistenceManagers.Where(pm => pm is SqlAzurePersistenceManager).FirstOrDefault();
                    break;
                case PersistenceTechnologies.BlobsStorageAzure:
                    persistenceManager = persistenceManagers.Where(pm => pm is BlobAzurePersistenceManager).FirstOrDefault();
                    break;
                case PersistenceTechnologies.TableStorageAzure:
                    persistenceManager = persistenceManagers.Where(pm => pm is TableStorageAzurePersistenceManager).FirstOrDefault();
                    break;
            }
            return persistenceManager;
        }
    }
}
