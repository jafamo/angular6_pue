﻿using Es.Pue.Encuestas.Model.EntitiesLayer.Entities.Authentications;
using Es.Pue.Encuestas.Model.PersistenceLayer.Managers;
using Es.Pue.Encuestas.Model.ServiceLayer.Api.Authentications;
using Es.Pue.Encuestas.Model.ServiceLayer.Impl.Bases;
using Es.Pue.Encuestas.UtilitiesLayer.Utilities.Cryptography;
using Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static Es.Pue.Encuestas.UtilitiesLayer.Utilities.Peristences.PersistenceUtilities;

namespace Es.Pue.Encuestas.Model.ServiceLayer.Impl.Authentications
{
    class LoginService : ServiceBase, ILoginService
    {
        public LoginService(List<PersistenceManager> persistenceManagers) : base(persistenceManagers)
        {
        }

        public async Task<Login> GetLoginByNameAndPassWord(string loginName, string password, PersistenceTechnologies persistenceTechnology)
        {

            var login= await GetPersistenceManager(persistenceTechnology).GetLoginDAO().GetLoginByLoginName(loginName);

            if (login == null || !SecureHasher.Verify(password,login.Password))
            {
                return null;
            }
            else {
                return login;
            }
            
        }
        
        public void SaveLogin(Login login, PersistenceTechnologies persistenceTechnology)
        {
            login.Password = SecureHasher.Hash(login.Password);

            GetPersistenceManager(persistenceTechnology).GetLoginDAO().SaveLogin(login);
            
        }
    }
}
